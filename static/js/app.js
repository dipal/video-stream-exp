/*
 * ASCII Camera
 * http://idevelop.github.com/ascii-camera/
 *
 * Copyright 2013, Andrei Gheorghe (http://github.com/idevelop)
 * Released under the MIT license
 */

(function() {
	var asciiContainer = document.getElementById("ascii");
	var targetCanvas = document.getElementById('canvas');
	var capturing = false;
	var fps = 25;
	var upload_duration = 5000;
	var upload_timer;
	var q = new CircularQueue(5);
	q.push(new Whammy.Video(fps));

	camera.init({
		width: 480,
		height: 360,
		fps: fps,
		mirror: false,
		targetCanvas: targetCanvas,

		onFrame: function(canvas) {
			q.getFront().add(canvas.getContext('2d'))
			// ascii.fromCanvas(canvas, {
			// 	// contrast: 128,
			// 	callback: function(asciiString) {
			// 		asciiContainer.innerHTML = asciiString;
			// 	}
			// });
		},

		onSuccess: function() {
			document.getElementById("info").style.display = "none";

			const button = document.getElementById("button");
			button.style.display = "block";
			button.onclick = function() {
				if (capturing) {
					camera.pause();
					button.innerText = 'resume';
				} else {
					
					camera.start();
					button.innerText = 'pause';
				}
				capturing = !capturing;
			};
		},

		onError: function(error) {
			// TODO: log error
		},

		onNotSupported: function() {
			document.getElementById("info").style.display = "none";
			asciiContainer.style.display = "none";
			document.getElementById("notSupported").style.display = "block";
		},

		onStartCapture: function() {
			console.log('on start capture');
			uploadTimer = setInterval(function(){
				whammyVideo = q.getFront();
				q.push(new Whammy.Video(fps));
				whammyVideo.compile(false, function(output){
					// var url = webkitURL.createObjectURL(output);
					window.output = output
					// document.getElementById('recorded_video').src = url; //toString converts it to a URL via Object URLs, falling back to DataUR
					console.log(output.size)
					document.output = output
					var fd = new FormData();
					fd.append('fname', 'video_stream.webm');
					fd.append('data', output);
					$.ajax({
					    type: 'POST',
					    url: '/video/stream',
					    data: fd,
					    processData: false,
					    contentType: false
					}).done(function(data) {
					       console.log(data);
					       // whammyVideo = new Whammy.Video(fps);
					});
				});
			}, upload_duration);
		},

		onPauseCapture: function() {
			console.log('on pause capture');
			if (uploadTimer) clearInterval(uploadTimer);
		}
	});
})();
