/*
	camera.js v1.1
	http://github.com/idevelop/camera.js

	Author: Andrei Gheorghe (http://idevelop.github.com)
	License: MIT
*/

var camera = (function() {
	var options;
	var video, canvas, context, canvasManual, contextManual;
	var renderTimer;

	function initVideoStream() {
		video = document.createElement("video");
		window.video = video
		// video = document.getElementById('videoContainer');
		video.setAttribute('width', options.width);
		video.setAttribute('height', options.height);
		video.setAttribute('playsinline', 'true');
		video.setAttribute('webkit-playsinline', 'true');

		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
		window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;

		if (navigator.getUserMedia) {
			navigator.getUserMedia({
				video: true,
				audio: false,
			}, function(stream) {
				options.onSuccess();

				if (video.mozSrcObject !== undefined) { // hack for Firefox < 19
					video.mozSrcObject = stream;
				} else {
					video.srcObject = stream;
				}

				initCanvas();
			}, options.onError);
		} else {
			options.onNotSupported();
		}
	}

	function initCanvas() {
		canvas = options.targetCanvas || document.createElement("canvas");
		canvas.setAttribute('width', options.width);
		canvas.setAttribute('height', options.height);

		context = canvas.getContext('2d');

		// mirror video
		if (options.mirror) {
			context.translate(canvas.width, 0);
			context.scale(-1, 1);
		}

		// canvasManual = document.getElementById("canvasManual");
		// canvasManual.setAttribute('width', options.width);
		// canvasManual.setAttribute('height', options.height);

		// contextManual = canvasManual.getContext('2d');
	}

	function startCapture() {
		video.play();

		renderTimer = setInterval(function() {
			try {
				context.drawImage(video, 0, 0, video.width, video.height);
				options.onFrame(canvas);
				// var img = context.getImageData(0, 0, video.width, video.height);
				// contextManual.drawImage(video, 0, 0, video.width, video.height);
				// var img = contextManual.getImageData(0, 0, video.width, video.height);
				// console.log(img);
			} catch (e) {
				// TODO
			}
		}, Math.round(1000 / options.fps));

		options.onStartCapture();
	}

	function stopCapture() {
		pauseCapture();

		if (video.mozSrcObject !== undefined) {
			video.mozSrcObject = null;
		} else {
			video.srcObject = null;
		}

		options.onStopCapture();
	}

	function pauseCapture() {
		if (renderTimer) clearInterval(renderTimer);
		video.pause();

		options.onPauseCapture();
	}

	return {
		init: function(captureOptions) {
			var doNothing = function(){};

			options = captureOptions || {};

			options.fps = options.fps || 30;
			options.width = options.width || 640;
			options.height = options.height || 480;
			options.mirror = options.mirror || false;
			options.targetCanvas = options.targetCanvas || null; // TODO: is the element actually a <canvas> ?

			options.onSuccess = options.onSuccess || doNothing;
			options.onError = options.onError || doNothing;
			options.onNotSupported = options.onNotSupported || doNothing;
			options.onFrame = options.onFrame || doNothing;
			options.onStartCapture = options.onStartCapture || doNothing;
			options.onPauseCapture = options.onPauseCapture || doNothing;
			options.onStopCapture = options.onStopCapture || doNothing;

			initVideoStream();
		},

		start: startCapture,

		pause: pauseCapture,

		stop: stopCapture
	};
})();
