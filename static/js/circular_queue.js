function CircularQueue(n) {
    this._array= new Array(n);
    this.length= 0;
}
CircularQueue.prototype.toString= function() {
    return '[object CircularQueue('+this._array.length+') length '+this.length+']';
};
CircularQueue.prototype.get= function(i) {
    if (i<0 || i<this.length-this._array.length)
        return undefined;
    return this._array[i%this._array.length];
};
CircularQueue.prototype.set= function(i, v) {
    if (i<0 || i<this.length-this._array.length)
        throw CircularQueue.IndexError;
    while (i>this.length) {
        this._array[this.length%this._array.length]= undefined;
        this.length++;
    }
    this._array[i%this._array.length]= v;
    if (i==this.length)
        this.length++;
};
CircularQueue.prototype.push = function(v) { this._array[this.length%this._array.length] = v; this.length++; };
CircularQueue.prototype.getFront = function() {
    return this.get(this.length-1);
};
CircularQueue.IndexError= {};

