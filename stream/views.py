from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import cv2

# Create your views here.


def index(request):

	context = {}
	return render(request, 'stream/index.html', context)

@csrf_exempt
def video_stream(request):
	if request.method != 'POST' or not request.FILES['data']:
		return JsonResponse({'success': False})

	video_file = request.FILES['data']
	print(video_file.temporary_file_path())
	
	cap = cv2.VideoCapture(video_file.temporary_file_path())
	if (cap.isOpened() == False): 
		print("Error opening video stream or file")

	frames = []
	while(cap.isOpened()):
		ret, frame = cap.read()
		if ret:
			# print (ret, frame)
			frames.append(frame)
		else:
			break

	print ('read', len(frames), 'frames')
	return JsonResponse({'success': True})