from django.urls import path

from . import views

app_name='stream'
urlpatterns = [
    path('', views.index, name='index'),
    path('video/stream', views.video_stream, name='video_stream'),
]
